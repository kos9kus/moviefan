//
//  MovieServiceImplTests.swift
//  MovieFanTests
//
//  Created by KONSTANTIN KUSAINOV on 01/10/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest

class MovieServiceImplTests: XCTestCase {

    func testFetchMovies() {
        let provider = AlamofireNetwork()
        let network = Network(networkProvider: provider)
        let jsonDecoder = JsonParser()
        let decoder = Parser(parserDecoder: jsonDecoder)
        let service: MovieService = MovieServiceImpl(networkPerfomer: network, parserPerfomer: decoder)
        
        let q = "Tt"
        let page = 1
        let exp = expectation(description: "testFetchMovies")
        service.fetchMovies(pattern: q, page: page, completion: { (moviePage) in
            XCTAssertTrue(moviePage.page == 1)
            XCTAssertTrue(moviePage.results.count > 0)
            exp.fulfill()
        }) { (error) in
            XCTFail(error.verboseMessage)
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 1)
    }
    
    func testFetchMoviesNoItems() {
        let provider = AlamofireNetwork()
        let network = Network(networkProvider: provider)
        let jsonDecoder = JsonParser()
        let decoder = Parser(parserDecoder: jsonDecoder)
        let service: MovieService = MovieServiceImpl(networkPerfomer: network, parserPerfomer: decoder)
        
        let q = "2342349023490" // test title
        let page = 1
        let exp = expectation(description: "testFetchMovies")
        service.fetchMovies(pattern: q, page: page, completion: { (moviePage) in
            XCTAssertTrue(moviePage.page == 1)
            XCTAssertTrue(moviePage.results.count == 0)
            exp.fulfill()
        }) { (error) in
            XCTFail(error.verboseMessage)
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 1)
    }

}
