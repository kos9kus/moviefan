//
//  JsonParserTests.swift
//  MovieFanTests
//
//  Created by KONSTANTIN KUSAINOV on 26/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest

class JsonParserTests: XCTestCase {

    func testParseMoviePage() {
        let parser = JsonParser()
		
        let dataStr = "{\"page\":1,\"total_results\":106,\"total_pages\":6,\"results\":[{\"id\":268, \"title\":\"Batman\", \"poster_path\":\"\\/kBf3g9crrADGMc2AMAMlLBgSm2h.jpg\",\"overview\":\"The Dark Knight of Gotham City begins his war on crime with his first major enemy being the clownishly homicidal Joker, who has seized control of Gotham's underworld.\",\"release_date\":\"1989-06-23\"}, {\"id\":268, \"title\":\"Batman\", \"poster_path\":\"\\/kBf3g9crrADGMc2AMAMlLBgSm2h.jpg\",\"overview\":\"The Dark Knight of Gotham City begins his war on crime with his first major enemy being the clownishly homicidal Joker, who has seized control of Gotham's underworld.\",\"release_date\":\"1989-06-23\"}]}"
		let testData = dataStr.data(using: .utf8)!
		parser.parseMoviePage(data: testData, completion: { (moviePage: MoviePage) in
            XCTAssertTrue(moviePage.page == 1)
            XCTAssertTrue(moviePage.totalResults == 106)
            XCTAssertTrue(moviePage.totalPages == 6)
            XCTAssertTrue(moviePage.results.count == 2)
            XCTAssertTrue(moviePage.results.first!.overview == "The Dark Knight of Gotham City begins his war on crime with his first major enemy being the clownishly homicidal Joker, who has seized control of Gotham's underworld.")
		}) { (status) in
            XCTFail("Error occured: \(status)")
		}
    }
    
//    func testParseMovie() {
//        let parser = JsonParser()
//
//        let dataStr = "{\"id\":268, \"title\":\"Batman\", \"poster_path\":\"\\/kBf3g9crrADGMc2AMAMlLBgSm2h.jpg\",\"overview\":\"The Dark Knight of Gotham City begins his war on crime with his first major enemy being the clownishly homicidal Joker, who has seized control of Gotham's underworld.\",\"release_date\":\"1989-06-23\"}"
//        let testData = dataStr.data(using: .utf8)!
//        parser.parseMoviePage(data: testData, completion: { (moviePage: Movie) in
//            XCTAssertTrue(moviePage.page == 1)
//            XCTAssertTrue(moviePage.totalResults == 106)
//            XCTAssertTrue(moviePage.totalPages == 6)
//        }) { (status) in
//            XCTFail("Error occured")
//        }
//    }

}
