//
//  AlamofireRequestTests.swift
//  MovieFanTests
//
//  Created by KONSTANTIN KUSAINOV on 25/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest

class AlamofireRequestTests: XCTestCase {
    
    func testRequest() {
        let queryStr = "batman"
        let page = 1
        let mockUrl = "http://api.themoviedb.org/3/search/movie?api_key=2696829a81b1b5827d515ff121700838&query=\(queryStr)&page=\(page)"
        let query = SearchQuery(query: queryStr, page: page)
        let request = AlamofireRequest.searchMovie(query)
        
        XCTAssertTrue(mockUrl == request.requestUrl, "requestUrl \(request.requestUrl)")
    }
    
    func testImageRequest() {
        let path = "2DtPSyODKWXluIRV7PVru0SSzja.jpg"
        let size = "92"
        let mockUrl = "http://image.tmdb.org/t/p/w\(size)/\(path)"
        let config = NetworkImageMainConfig()
        let method = ProcedureMethod.image(size: size, imagePath: path)
        let request = AlamofireImageRequest(config: config, procedure: method)
        XCTAssertTrue(mockUrl == request.requestUrl, "requestUrl \(request.requestUrl)")
    }
}
