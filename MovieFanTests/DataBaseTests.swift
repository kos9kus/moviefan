//
//  DataBaseTests.swift
//  MovieFanTests
//
//  Created by KONSTANTIN KUSAINOV on 06/10/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest
import RealmSwift
@testable import MovieFan

class DataBaseTests: XCTestCase {
    
    let realm = try! Realm()
    
    override func setUp() {
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    struct SearchEntryDTO: SearchEntry {
        var entry: String
        var dateCreated = Date()
        var successful: Bool = true
        
        init(search: String) {
            entry = search
        }
        
        init(search: String, date: Date) {
            entry = search
            dateCreated = date
        }
    }
    
    func testFetchAllObject() {
        let db: DataBase = RealmDataBase()
        let search = "Batman"
        let entryBatman: SearchEntry = SearchEntryDTO(search: search)
        db.addSearchEntry(entry: entryBatman)
        let exp = expectation(description: "testFetchAllObject")
        let token = realm.observe { notification, realm in
            db.allSearchEnries(query: "", completion: { (entry) in
                XCTAssertTrue(entry.count == 1)
                exp.fulfill()
            })
        }
        self.waitForExpectations(timeout: 2)
        token.invalidate()
    }

    func testFetchAllObjectWithPattern() {
        let db: DataBase = RealmDataBase()
        let search = "Batman"
        let entryBatman: SearchEntry = SearchEntryDTO(search: search)
        db.addSearchEntry(entry: entryBatman)
        
        let exp = expectation(description: "testFetchAllObject")
        let token = realm.observe { notification, realm in
            db.allSearchEnries(query: "Bat", completion: { (entry) in
                let filteredEntries = entry.filter({ $0.entry == entryBatman.entry})
                XCTAssertTrue(filteredEntries.count == 1)
                exp.fulfill()
            })
        }
        
        self.waitForExpectations(timeout: 2)
        token.invalidate()
    }
    
    func testSortingOrderFetchAllObject() {
        let db: DataBase = RealmDataBase()
        let search1 = "Batman1"
        let entryBatman1: SearchEntry = SearchEntryDTO(search: search1, date: Date(timeIntervalSinceNow: -20))
        db.addSearchEntry(entry: entryBatman1)
        let search2 = "Batman2"
        let entryBatman2: SearchEntry = SearchEntryDTO(search: search2, date: Date(timeIntervalSinceNow: 20))
        db.addSearchEntry(entry: entryBatman2)
        let search3 = "Batman3"
        let entryBatman3: SearchEntry = SearchEntryDTO(search: search3)
        db.addSearchEntry(entry: entryBatman3)
        
        let exp = expectation(description: "testSortingOrderFetchAllObject")
        let search = "Bat"
        var complete = false
        let token = realm.observe { notification, realm in
            db.allSearchEnries(query: search, completion: { (entries) in
                guard entries.count == 3 && complete == false else { return }
                XCTAssertTrue(entries[0] == entryBatman2)
                XCTAssertTrue(entries[1] == entryBatman3)
                XCTAssertTrue(entries[2] == entryBatman1)
                complete = true
                exp.fulfill()
            })
        }
        
        self.waitForExpectations(timeout: 2)
        token.invalidate()
    }
}

func ==(lhs: SearchEntry, rhs: SearchEntry) -> Bool {
    return lhs.entry == rhs.entry
}

