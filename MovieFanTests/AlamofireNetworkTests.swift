//
//  AlamofireNetworkTests.swift
//  MovieFanTests
//
//  Created by KONSTANTIN KUSAINOV on 25/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest

class AlamofireNetworkTests: XCTestCase {

    func testMakeRequest() {
        struct AlamofireRequestMock: AFRRequest {
            var requestUrl: String {
                return "http://api.themoviedb.org/3/search/movie?api_key=2696829a81b1b5827d515ff121700838&query=batman&page=1"
            }
        }
        let exp = expectation(description: "testMakeGetRequest")
        let network = AlamofireNetwork()
        network.makeRequest(request: AlamofireRequestMock(), completion: { (data: Data) in
            XCTAssert(data.count > 0)
            exp.fulfill()
        }) { (error) in
            XCTAssert(false, "Error occurred")
        }
        
        waitForExpectations(timeout: 2)
    }
    
    func testMakeGetRequestFailure() {
        struct AlamofireRequestMock: AFRRequest {
            
            // api_key is missing
            // wating for 401 status
            var requestUrl: String {
                return "http://api.themoviedb.org/3/search/movie?query=batman&page=1"
            }
        }
        let exp = expectation(description: "testMakeGetRequest")
        let network = AlamofireNetwork()
        network.makeRequest(request: AlamofireRequestMock(), completion: { (data: Data) in
            XCTAssert(false, "Error occurred")
        }) { (error) in
            if case .badResponse = error {
            } else {
                XCTAssert(false, "error is incorrect")
            }
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 2)
    }
    
    func testMakeImageRequest() {
        struct AlamofireRequestMock: AFRRequest {
            var requestUrl: String {
                return "http://image.tmdb.org/t/p/w92/2DtPSyODKWXluIRV7PVru0SSzja.jpg"
            }
        }
        
        let exp = expectation(description: "testMakeImageRequest")
        let network = AlamofireNetwork()
        
        network.makeRequest(request: AlamofireRequestMock(), completion: { (image: UIImage) in
            exp.fulfill()
        }) { (error) in
            XCTFail()
        }
        
        waitForExpectations(timeout: 2)
    }
    
    func testMakeImageRequestNoImageFound() {
        struct AlamofireRequestMock: AFRRequest {
            var requestUrl: String {
                return "http://image.tmdb.org/t/p/w92/testMakeImageRequestNoImageFound.jpg"
            }
        }
        
        let exp = expectation(description: "testMakeImageRequest")
        let network = AlamofireNetwork()
        
        network.makeRequest(request: AlamofireRequestMock(), completion: { (image: UIImage) in
            XCTFail()
            exp.fulfill()
        }) { (error) in
            if case .badResponse = error {
            } else {
                XCTAssert(false, "error is incorrect")
            }
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 2)
    }

}
