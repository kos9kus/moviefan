//
//  ParserTests.swift
//  MovieFanTests
//
//  Created by k.kusainov on 25/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest

class ParserTests: XCTestCase {
	
	class MockDecoder: ParserDecoder {
		
		typealias DecoderErrorStatus = MockDecoderErrorStatus
		
		enum MockDecoderErrorStatus {
			case invalidData
		}
		
		func parseMoviePage(data: Data, completion: @escaping (String) -> (), failure: (DecoderErrorStatus) -> ()) {
			let decodedData = String(data: data, encoding: .utf8)
			if let data = decodedData, data.count > 0 {
				completion(data)
			} else {
				failure(.invalidData)
			}
		}
	}

	func testParseData() {
		let parser = Parser(parserDecoder: MockDecoder())
		let testDataStr = "testParseData"
		let testData = testDataStr.data(using: .utf8)!
		let exp = expectation(description: "testParseData")
		parser.parseMoviePage(data: testData, completion: { (data) in
			XCTAssertTrue(testDataStr == data)
			exp.fulfill()
		}) { (error) in
		}
		waitForExpectations(timeout: 1)
    }
	
	func testParseDataFailure() {
		let parser = Parser(parserDecoder: MockDecoder())
		let testData = Data()
		let exp = expectation(description: "testParseData")
		parser.parseMoviePage(data: testData, completion: { (data) in
		}) { (error) in
			exp.fulfill()
		}
		waitForExpectations(timeout: 1)
	}

}
