//
//  ProcedureMethodTests.swift
//  MovieFanTests
//
//  Created by KONSTANTIN KUSAINOV on 25/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest

class ProcedureMethodTests: XCTestCase {

    func testMethodSearchMovie() {
        let searchMovieMethod = ProcedureMethod.search(.movie)
        let mockMethod = "/3/search/movie"
        XCTAssertTrue(searchMovieMethod.url == mockMethod)
    }
    
    func testMethodImage() {
        let path = "2DtPSyODKWXluIRV7PVru0SSzja.jpg​"
        let size = "92"
        let method = ProcedureMethod.image(size: size, imagePath: path)
        let mockMethod = "/t/p/w92/2DtPSyODKWXluIRV7PVru0SSzja.jpg​"
        XCTAssertTrue(method.url == mockMethod)
    }

}
