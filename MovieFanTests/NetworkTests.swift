//
//  NetworkTests.swift
//  MovieFanTests
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest

class NetworkTests: XCTestCase {

    func testMakeGetRequestWithData() {
        let exp = expectation(description: "testMakeGetRequestWithData")
        let dataTest = Data()
        let network = Network(networkProvider: NetworkProviderMock(dataMock: dataTest))
        network.makeRequest(request: RequestMock(), completion: { (data: Data) in
            exp.fulfill()
        }) { (error) in }
        waitForExpectations(timeout: 1)
    }
    
    func testMakeGetRequestWithClientError() {
        let exp = expectation(description: "testMakeGetRequestWithClientError")
        let response404 = HTTPURLResponse(url: URL(fileURLWithPath: ""), statusCode: 404, httpVersion: nil, headerFields: nil)
        let network = Network(networkProvider: NetworkProviderMock(errorMock: .badResponse(response404!)))
        network.makeRequest(request: RequestMock(), completion: { (data: Data) in
        }) { (error) in
            if case .clientError = error {
            } else {
                XCTFail("Incorrect type response")
            }
            exp.fulfill()
        }
        waitForExpectations(timeout: 1)
    }
}

struct RequestMock {}

class NetworkProviderMock: NetworkProvider {
    
    typealias Request = RequestMock

    let data: Data?
    let error: NetworkErrorResponse?
    
    init(dataMock: Data? = nil, errorMock: NetworkErrorResponse? = nil) {
        data = dataMock
        error = errorMock
    }
    
    func makeRequest(request: RequestMock, completion: @escaping (Data) -> (), failure: @escaping (NetworkErrorResponse) -> ()) {
        if let d = data {
            completion(d)
        } else {
            failure(error!)
        }
    }
    
    func makeRequest(request: RequestMock, completion: @escaping (UIImage) -> (), failure: @escaping (NetworkErrorResponse) -> ()) {
        
    }

}
