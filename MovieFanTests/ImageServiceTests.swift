//
//  ImageServiceTests.swift
//  MovieFanTests
//
//  Created by KONSTANTIN KUSAINOV on 05/10/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest

class ImageServiceTests: XCTestCase {

    func testImage() {
        let config: ServerNetworkConfig = NetworkImageMainConfig()
        let network: Network = Network(networkProvider: AlamofireNetwork())
        let service:ImageService = ImageServiceImpl(networkPerfomer: network, networkConfig: config)
        let imgParam = ImageParam.init(path: "JJCoNs7qbc576alCLgg1E97rfn.jpg", size: "92")
        
        let exp = expectation(description: "testImage")
        service.image(imageParam: imgParam, completion: { (image) in
            exp.fulfill()
        }) { (error) in
            XCTFail(error.message)
        }
        
        self.waitForExpectations(timeout: 1)
    }
    
    func testImageNotFound() {
        let config: ServerNetworkConfig = NetworkImageMainConfig()
        let network: Network = Network(networkProvider: AlamofireNetwork())
        let service:ImageService = ImageServiceImpl(networkPerfomer: network, networkConfig: config)
        let imgParam = ImageParam.init(path: "testImageNotFound.jpg", size: "92")
        let exp = expectation(description: "testImageNotFound")
        service.image(imageParam: imgParam, completion: { (image) in
            XCTFail("Image is found")
        }) { (error) in
            exp.fulfill()
        }
        
        self.waitForExpectations(timeout: 1)
    }

}
