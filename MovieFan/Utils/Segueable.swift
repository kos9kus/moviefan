//
//  Segueable.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 08/10/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation
import UIKit

protocol Segueable {
    associatedtype SegueIdentifier: RawRepresentable
}

extension Segueable where Self: UIViewController, SegueIdentifier.RawValue == String {
    func segueIdentifier(for segue: UIStoryboardSegue) -> SegueIdentifier {
        guard let identifier = segue.identifier,
            let segueIdentifier = SegueIdentifier(rawValue: identifier) else {
                fatalError("Segue was not found  for view controller: \(Self.self))")
        }
        return segueIdentifier
    }
}
