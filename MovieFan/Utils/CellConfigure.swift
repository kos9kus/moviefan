//
//  CellConfigure.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 07/10/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol CellConfigure {
    associatedtype Model
    func configureForCell(model: Model)
    static var cellReuseIdentifier: String { get }
}

extension CellConfigure {
    static var cellReuseIdentifier: String {
        return String(describing: Self.self)
    }
}
