//
//  Date+Format.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 27/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

enum DateError: LocalizedError {
    case incorectParms
    
    var errorDescription: String? {
        switch self {
        case .incorectParms:
            return NSLocalizedString("Incorrect params", comment: "")
        }
    }
}

extension Date {
    static func dateFromStringFrom(format pattern: String, date dateString: String) throws -> Date  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = pattern
        guard let date = dateFormatter.date(from: dateString) else  {
            throw DateError.incorectParms
        }
        return date
    }
    
    func stringDateFull(format pattern: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = pattern
        return dateFormatter.string(from: self)
    }
}
