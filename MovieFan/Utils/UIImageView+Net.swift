//
//  UIImageView+Net.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 08/10/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

extension UIImageView {
    
    // TODO: setting service from out of class (remove dependencies from concrete classes) or `ImageService` should be singleton
    private var service: ImageService {
        let config: ServerNetworkConfig = NetworkImageMainConfig()
        let network: Network = Network(networkProvider: AlamofireNetwork())
        let service: ImageService = ImageServiceImpl(networkPerfomer: network, networkConfig: config)
        return service
    }
    
    func net_setImage(imageParam: ImageParam) {
        service.image(imageParam: imageParam, completion: { [weak self] (downloadedImage) in
            self?.image = downloadedImage
        }) { [weak self] (error) in
            // TODO: make setting placeholder from out of class
            self?.image = UIImage(named: "noImageIcon")
            print(error.message)
        }
    }
}

