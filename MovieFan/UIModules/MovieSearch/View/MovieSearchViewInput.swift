//
//  MovieSearchMovieSearchViewInput.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

protocol MovieSearchViewInput: class {

    /**
        @author KONSTANTIN KUSAINOV
        Setup initial state of the view
    */

    func setupInitialState()
    func hideProgressIndicator(hidden: Bool)
    func reloadData()
}
