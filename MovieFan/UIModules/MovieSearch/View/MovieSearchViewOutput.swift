//
//  MovieSearchMovieSearchViewOutput.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

protocol MovieSearchDelegate: class {
    func didSearchMovies(moviePage: MoviePage, searchEntry: String)
    func didSelectSearchEntry(searchEntry: String)
}

protocol MovieSearchViewOutput {

    /**
        @author KONSTANTIN KUSAINOV
        Notify presenter that view is ready
    */
    
    var delegate: MovieSearchDelegate? { get set }

    func viewIsReady()
    func didChangeSearch(pattern: String)
    func didSearchTap(pattern: String)
    func didSearchTap()
    func didSelectRow(at index: Int)
    func numberOfItems() -> Int
    func modelAtIndex(index: Int) -> SearchEntry
}
