//
//  SearchTableViewCell.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 07/10/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell, CellConfigure {
    typealias Model = SearchEntry
    
    func configureForCell(model: SearchEntry) {
        self.textLabel?.text = model.entry
    }
}
