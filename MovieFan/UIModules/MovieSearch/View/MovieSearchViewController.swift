//
//  MovieSearchMovieSearchViewController.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

import UIKit

class MovieSearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MovieSearchViewInput, UserErrorPresentable, UISearchBarDelegate {

    var output: MovieSearchViewOutput!
    @IBOutlet var tableView: UITableView!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

    // MARK: MovieSearchViewInput
    
    func setupInitialState() {
    }
    
    func hideProgressIndicator(hidden: Bool) {
        // TODO: make the method via protocol
    }
    
    func reloadData() {
        self.tableView.reloadData()
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.numberOfItems()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchTableViewCell.cellReuseIdentifier) as? SearchTableViewCell else {
            fatalError("SearchTableViewCell was not instantiated")
        }
        
        let model = output.modelAtIndex(index: indexPath.row)
        cell.configureForCell(model: model)
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output.didSelectRow(at: indexPath.row)
    }
    
    // MARK: UISearchBarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        output.didChangeSearch(pattern: searchBar.text ?? "")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text, text.count > 0 {
            output.didSearchTap(pattern: text)
        } else {
            output.didSearchTap()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        output.didSearchTap()
    }
}
