//
//  MovieSearchMovieSearchInitializer.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

import UIKit

class MovieSearchModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var moviesearchViewController: MovieSearchViewController!

    override func awakeFromNib() {

        let configurator = MovieSearchModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: moviesearchViewController)
    }

}
