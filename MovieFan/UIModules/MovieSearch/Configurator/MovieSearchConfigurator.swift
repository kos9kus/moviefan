//
//  MovieSearchMovieSearchConfigurator.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

import UIKit

class MovieSearchModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? MovieSearchViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: MovieSearchViewController) {

        let router = MovieSearchRouter(viewController: viewController)

        let presenter = MovieSearchPresenter()
        presenter.view = viewController
        presenter.router = router

        let dataBase = RealmDataBase()
        let movieService = MovieServiceImpl.createMovieService()
        let interactor = MovieSearchInteractor(searchDataBase: dataBase, movieService: movieService)
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
