//
//  MovieSearchMovieSearchRouter.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

import UIKit

class MovieSearchRouter: MovieSearchRouterInput {
    private weak var view: UserErrorPresentable!
    
    init(viewController: UserErrorPresentable) {
        view = viewController
    }
    
    func presentError(error: ErrorDescriptor) {
        view.presentError(error: error)
    }
}
