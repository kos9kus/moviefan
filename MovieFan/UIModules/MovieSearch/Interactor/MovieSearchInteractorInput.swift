//
//  MovieSearchMovieSearchInteractorInput.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

import Foundation

protocol MovieSearchInteractorInput {
    var items: Array<SearchEntry> { get }
    func fetch(searchEntry: String)
    func search(searchEntry: String)
    func saveEntry(entry: SearchEntry)
}
