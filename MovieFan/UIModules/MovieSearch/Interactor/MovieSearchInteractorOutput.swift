//
//  MovieSearchMovieSearchInteractorOutput.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

import Foundation

protocol MovieSearchInteractorOutput: class {
    func didFetchData()
    func didSearch(error: ErrorDescriptor)
    func didSearch(search: String, result: MoviePage)
}
