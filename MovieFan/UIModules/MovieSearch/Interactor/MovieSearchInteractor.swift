//
//  MovieSearchMovieSearchInteractor.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

import Foundation

class MovieSearchInteractor: MovieSearchInteractorInput {

    weak var output: MovieSearchInteractorOutput!
    private let dataBase: DataBase
    private let service: MovieService
    private(set) var items: Array<SearchEntry> = []
    
    init(searchDataBase: DataBase, movieService: MovieService) {
        dataBase = searchDataBase
        service = movieService
    }
    
    // MARK: MovieSearchInteractorInput
    
    func fetch(searchEntry: String) {
        dataBase.allSearchEnries(query: searchEntry) { [weak self] (entries) in
            DispatchQueue.main.async {
                self?.items = entries
                self?.output.didFetchData()
            }
        }
    }
    
    func saveEntry(entry: SearchEntry) {
        dataBase.addSearchEntry(entry: entry)
    }
    
    func search(searchEntry: String) {
        service.fetchMovies(pattern: searchEntry, page: 1, completion: { [weak self] (movie) in
            DispatchQueue.main.async {
                if movie.results.count > 0 {
                    self?.output.didSearch(search: searchEntry, result: movie)
                } else {
                    self?.output.didSearch(error: NoItemsError())
                }
            }
        }) { [weak self]  (error) in
            DispatchQueue.main.async {
                self?.output.didSearch(error: error)
            }
        }
    }
}

fileprivate struct NoItemsError: ErrorDescriptor {
    var title: String {
        return "No found movies"
    }
    
    var message: String {
        return "Change your request"
    }
}
