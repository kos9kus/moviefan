//
//  MovieSearchMovieSearchPresenter.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

import Foundation

class MovieSearchPresenter: MovieSearchViewOutput, MovieSearchInteractorOutput {
    
    weak var delegate: MovieSearchDelegate?
    weak var view: MovieSearchViewInput!
    var interactor: MovieSearchInteractorInput!
    var router: MovieSearchRouterInput!
    
    private func fetchAllEntries() {
        view.hideProgressIndicator(hidden: false)
        interactor.fetch(searchEntry: "")
    }
    
    // MARK: MovieSearchViewOutput

    func viewIsReady() {
        fetchAllEntries()
    }
    
    func didSearchTap() {
        fetchAllEntries()
    }
    
    func didChangeSearch(pattern: String) {
        view.hideProgressIndicator(hidden: false)
        interactor.fetch(searchEntry: pattern)
    }
    
    func didSearchTap(pattern: String) {
        self.view.hideProgressIndicator(hidden: false)
        interactor.search(searchEntry: pattern)
    }
    
    func numberOfItems() -> Int {
        return interactor.items.count
    }
    
    func modelAtIndex(index: Int) -> SearchEntry {
        return interactor.items[index]
    }
    
    func didSelectRow(at index: Int) {
        let entry = modelAtIndex(index: index)
        delegate?.didSelectSearchEntry(searchEntry: entry.entry)
    }
    
    // MARK: MovieSearchInteractorOutput
    
    func didFetchData() {
        self.view.hideProgressIndicator(hidden: true)
        self.view.reloadData()
    }
    
    func didSearch(search: String, result: MoviePage) {
        let entry = MovieSearchEntry(entrySearch: search)
        interactor.saveEntry(entry: entry)
        delegate?.didSearchMovies(moviePage: result, searchEntry: search)
    }
    
    func didSearch(error: ErrorDescriptor) {
        self.router.presentError(error: error)
    }
}

fileprivate struct MovieSearchEntry: SearchEntry {
    var entry: String
    var dateCreated = Date()
    var successful = true
    
    init(entrySearch: String) {
        entry = entrySearch
    }
}
