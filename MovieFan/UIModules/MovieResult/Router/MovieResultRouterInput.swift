//
//  MovieResultMovieResultRouterInput.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

import Foundation

protocol MovieResultRouterInput {
    func popViewController()
    func presentError(error: ErrorDescriptor)
}
