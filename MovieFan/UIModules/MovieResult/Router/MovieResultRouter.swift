//
//  MovieResultMovieResultRouter.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

import UIKit

class MovieResultRouter: MovieResultRouterInput {
    
    weak var view: MovieResultViewController!
    
    init(viewController: MovieResultViewController) {
        view = viewController
    }
    
    func popViewController() {
        view.navigationController?.popViewController(animated: true)
    }
    
    func presentError(error: ErrorDescriptor) {
        view.presentError(error: error)
    }
}
