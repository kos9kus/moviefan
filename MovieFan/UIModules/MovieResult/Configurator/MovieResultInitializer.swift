//
//  MovieResultMovieResultInitializer.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

import UIKit

class MovieResultModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var movieresultViewController: MovieResultViewController!

    override func awakeFromNib() {

        let configurator = MovieResultModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: movieresultViewController)
    }

}
