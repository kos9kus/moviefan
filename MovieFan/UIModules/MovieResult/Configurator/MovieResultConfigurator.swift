//
//  MovieResultMovieResultConfigurator.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

import UIKit

class MovieResultModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? MovieResultViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: MovieResultViewController) {

        let router = MovieResultRouter(viewController: viewController)

        let presenter = MovieResultPresenter()
        presenter.view = viewController
        presenter.router = router

		let movieService = MovieServiceImpl.createMovieService()
		let interactor = MovieResultInteractor(movieService: movieService)
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
