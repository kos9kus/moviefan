//
//  MovieResultMovieResultViewController.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

import UIKit

class MovieResultViewController: UITableViewController, MovieResultViewInput, Segueable, UserErrorPresentable {
    
    enum SegueIdentifier: String {
        case showSearchViewController = "showSearchViewController"
    }

    var output: MovieResultViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let viewControllerType = segueIdentifier(for: segue)
        switch viewControllerType {
        case .showSearchViewController:
            guard let searchViewController = segue.destination as? MovieSearchViewController else {
                fatalError("Can't find viewController for type: \(viewControllerType.rawValue)")
            }
            searchViewController.output.delegate = output
            output.searchViewControllerWillShow()
            break
        }
    }


    // MARK: MovieResultViewInput
    func hideProgressIndicator(hidden: Bool) {
        // TODO:
    }
    
    func reloadData() {
        self.tableView.reloadData()
    }
    
    // MARK: UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.numberOfItems()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MovieResultTableViewCell.cellReuseIdentifier) as? MovieResultTableViewCell else {
            fatalError("SearchTableViewCell was not instantiated")
        }
        
        let model = output.modelAtIndex(index: indexPath.row)
        cell.configureForCell(model: model)
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        output.cellWillDisplay(at: indexPath.row)
    }
}


