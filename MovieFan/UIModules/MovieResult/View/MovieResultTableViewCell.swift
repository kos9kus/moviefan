//
//  MovieResultTableViewCell.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 07/10/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class MovieResultTableViewCell: UITableViewCell, CellConfigure {

    typealias Model = Movie
    
    @IBOutlet var movieImageView: UIImageView!
    @IBOutlet var movieTitle: UILabel!
    @IBOutlet var releaseDate: UILabel!
    @IBOutlet var overview: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        releaseDate.text = NSLocalizedString("Date unavailable", comment: "")
        movieImageView.image = UIImage(named: "noImageIcon")
    }

    func configureForCell(model: Movie) {
        movieTitle.text = model.title
        overview.text = model.overview
        if let date = model.releaseDate {
            releaseDate.text = date.stringDateFull(format: "d MMMM yyyy")
        }
        
        if let posterPath = model.posterPath {
            let size = Int(movieImageView.bounds.width)
            movieImageView.net_setImage(imageParam: ImageParam(path: posterPath, size: "\(size)"))
        }
    }
}
