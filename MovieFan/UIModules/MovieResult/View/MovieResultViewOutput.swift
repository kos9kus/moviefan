//
//  MovieResultMovieResultViewOutput.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

protocol MovieResultViewOutput: MovieSearchDelegate {

    /**
        @author KONSTANTIN KUSAINOV
        Notify presenter that view is ready
    */

    func searchViewControllerWillShow()
    func cellWillDisplay(at index: Int)
    func numberOfItems() -> Int
    func modelAtIndex(index: Int) -> Movie
}
