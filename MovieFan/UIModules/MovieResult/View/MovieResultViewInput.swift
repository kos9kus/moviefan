//
//  MovieResultMovieResultViewInput.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

protocol MovieResultViewInput: class {

    /**
        @author KONSTANTIN KUSAINOV
        Setup initial state of the view
    */

    func hideProgressIndicator(hidden: Bool)
    func reloadData()
}
