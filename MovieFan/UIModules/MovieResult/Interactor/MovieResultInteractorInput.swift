//
//  MovieResultMovieResultInteractorInput.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

import Foundation

protocol MovieResultInteractorInput {
    var items: Array<Movie> { get }
    
    func setNewMoviePage(moviePage: MoviePage, search: String)
    func startSearchMoviePage(at search: String)
    func searchNextPage()
    func resetState()
}
