//
//  MovieResultMovieResultInteractor.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

import Foundation

class MovieResultInteractor: MovieResultInteractorInput {

    weak var output: MovieResultInteractorOutput!
    private let service: MovieService
    private var status: SearchingStatus = SearchingStatus()
    private(set) var items: Array<Movie> = []

    init(movieService: MovieService) {
        service = movieService
    }
    
    func resetState() {
        items = []
    }
    
    func setNewMoviePage(moviePage: MoviePage, search: String) {
        status = SearchingStatus()
        status.pattern = search
        status.start()
        status.stop(totalPages: moviePage.totalPages)
        items.append(contentsOf: moviePage.results)
        output.didSearchMovies()
    }
    
    func startSearchMoviePage(at search: String) {
        status = SearchingStatus()
        status.pattern = search
        searchNextPage()
    }
    
    func searchNextPage() {
        if !status.isCanProceed {
            return
        }
        status.start()
        
        service.fetchMovies(pattern: status.pattern, page: status.pageNumber, completion: { [weak self] (moviePage) in
            DispatchQueue.main.async {
                self?.status.stop(totalPages: moviePage.totalPages)
                self?.items.append(contentsOf: moviePage.results)
                self?.output.didSearchMovies()
            }
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.status.stopFail()
                self?.output.didSearchMovies(error: error)
            }
        }
    }
}

fileprivate class SearchingStatus {
    var pattern: String = ""
    var pageNumber: Int = 0
    
    private var inProgress = false
    private var stopSearching = false
    
    func start() {
        inProgress = true
        pageNumber += 1
    }
    
    func stop(totalPages: Int) {
        inProgress = false
        stopSearching = pageNumber == totalPages
    }
    
    func stopFail() {
        inProgress = false
        pageNumber -= 1
    }
    
    var isCanProceed: Bool {
        return !inProgress && !stopSearching
    }
}
