//
//  MovieResultMovieResultPresenter.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KK. All rights reserved.
//

class MovieResultPresenter: MovieResultViewOutput, MovieResultInteractorOutput {

    weak var view: MovieResultViewInput!
    var interactor: MovieResultInteractorInput!
    var router: MovieResultRouterInput!

    func searchViewControllerWillShow() {
    }
    
    func cellWillDisplay(at index: Int) {
        if index == interactor.items.count - 1 {
            interactor.searchNextPage()
        }
    }
    
    func numberOfItems() -> Int {
        return interactor.items.count
    }
    
    func modelAtIndex(index: Int) -> Movie {
        return interactor.items[index]
    }
    
    // MARK: MovieResultInteractorOutput
    
    func didSearchMovies() {
        view.hideProgressIndicator(hidden: true)
        view.reloadData()
    }
    
    func didSearchMovies(error: ErrorDescriptor) {
        view.hideProgressIndicator(hidden: true)
        router.presentError(error: error)
    }
    
    // MARK: MovieSearchDelegate
    
    func didSearchMovies(moviePage: MoviePage, searchEntry: String)
    {
        router.popViewController()
        interactor.resetState()
        view.reloadData()
        interactor.setNewMoviePage(moviePage: moviePage, search: searchEntry)
    }
    
    func didSelectSearchEntry(searchEntry: String) {
        router.popViewController()
        interactor.resetState()
        view.reloadData()
        view.hideProgressIndicator(hidden: false)
        interactor.startSearchMoviePage(at: searchEntry)
    }
}
