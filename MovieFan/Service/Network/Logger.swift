//
//  Logger.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol Loggable {
    associatedtype Request
    func networkLog(request: Request, response: HTTPURLResponse?, data: Data?, error: Error?)
}
