//
//  NetworkRequest.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

enum ProcedureMethod: NetworkMethodUrl {
    case search(ProceedureMethodType)
    case image(size: String, imagePath: String)
    
    enum ProceedureMethodType {
        case movie
        case song
        case podcast
    }
    
    var url: String {
        switch self {
        case .search(let type):
            return "/3/search" + "/" + type.url
        case .image(let size, let imagePath):
            return "/t/p/w\(size)/\(imagePath)"
        }
    }
}

extension ProcedureMethod.ProceedureMethodType: NetworkMethodUrl {
    var url: String {
        switch self {
        case .movie:
            return "movie"
        default:
            return ""
        }
    }
}
