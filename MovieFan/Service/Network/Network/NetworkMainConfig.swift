//
//  NetworkMainConfig.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 25/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

struct NetworkMainConfig: MovieNetworkRequestParams, ServerNetworkConfig {
    var serverDomain: String {
        return "api.themoviedb.org"
    }
    
    var apiKey: String {
        return "2696829a81b1b5827d515ff121700838"
    }
}

struct NetworkMainQuery: Query {
    let queryItems: Array<URLQueryItem>
    
    init(params: MovieNetworkRequestParams) {
        queryItems = [
            URLQueryItem.init(name: "api_key", value:params.apiKey),
        ]
    }
}

struct NetworkImageMainConfig: ServerNetworkConfig {
    var serverDomain: String {
        return "image.tmdb.org"
    }
}

