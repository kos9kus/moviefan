//
//  NetworkConfig.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol ServerNetworkConfig {
    var serverDomain: String { get }
}

protocol MovieNetworkRequestParams {
    var apiKey: String { get }
}

protocol NetworkMethodUrl {
    var url: String { get }
}

struct RequestComponentsConstructor {
    static func request(server: ServerNetworkConfig, urlMethod: NetworkMethodUrl, query: Query? = nil) -> NSURLComponents {
        let components = NSURLComponents()
        components.scheme = "http"
        components.host = server.serverDomain
        components.path = urlMethod.url
        
        if let query = query {        
            components.queryItems = query.queryItems
        }
        return components
    }
    
}
