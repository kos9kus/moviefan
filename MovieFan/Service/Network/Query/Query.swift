//
//  Query.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 25/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol Query {
    var queryItems: Array<URLQueryItem> { get }
}
