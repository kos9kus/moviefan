//
//  SearchQuery.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 25/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

struct SearchQuery: Query {
    let query: String
    let page: Int
    
    var queryItems: Array<URLQueryItem> {
        return [
            URLQueryItem.init(name: "query", value:query),
            URLQueryItem.init(name: "page", value:"\(page)")
        ]
    }
}
