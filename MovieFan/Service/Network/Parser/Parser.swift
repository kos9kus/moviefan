//
//  Parser.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol ParserDecoder {
	associatedtype DecoderErrorStatus
	associatedtype ObjectMoviePage
	func parseMoviePage(data: Data, completion: @escaping (ObjectMoviePage) -> (), failure: (DecoderErrorStatus) -> ())
}

final class Parser<Decoder: ParserDecoder> {
	private let decoder: Decoder
	
	init(parserDecoder: Decoder) {
		decoder = parserDecoder
	}
	
	func parseMoviePage(data: Data, completion: @escaping (Decoder.ObjectMoviePage) -> (), failure: (Decoder.DecoderErrorStatus) -> ()) {
		decoder.parseMoviePage(data: data, completion: completion, failure: failure)
	}
}
