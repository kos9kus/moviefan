//
//  JsonParser.swift
//  MovieFan
//
//  Created by k.kusainov on 25/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

final class JsonParser: ParserDecoder {
	typealias DecoderErrorStatus = JsonParserErrorStatus

    enum JsonParserErrorStatus: ErrorDescriptor {
        
		case invalidData(Error)
        
        var title: String {
            switch self {
            case .invalidData(_):
                return NSLocalizedString("Decode data error", comment: "")
            }
        }
        
        var message: String {
            switch self {
            case .invalidData(_):
                return NSLocalizedString("Unable to decode data", comment: "")
            }
        }
        
        var verboseMessage: String {
            switch self {
            case .invalidData(let error):
                return error.localizedDescription
            }
        }
	}
	
	private let jsonDecoder = JSONDecoder()
	
	func parseMoviePage(data: Data, completion: @escaping (MoviePageImpl) -> (), failure: (JsonParser.JsonParserErrorStatus) -> ()) {
		self.parseData(data: data, completion: completion, failure: failure)
	}
	
	private func parseData<T: Decodable>(data: Data, completion: (T) -> (), failure: (JsonParser.JsonParserErrorStatus) -> ()) {
		do {
			let parsedData = try jsonDecoder.decode(T.self, from: data)
			completion(parsedData)
		} catch let e {
			failure(.invalidData(e))
		}
	}
}

