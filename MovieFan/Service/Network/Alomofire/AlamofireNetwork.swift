//
//  AlamofireNetwork.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

protocol AFRRequest {
    /// Full request with params
    var requestUrl: String { get }
}

class AlamofireNetwork: NetworkProvider {
    typealias Request = AFRRequest
    
    func makeRequest(request: AFRRequest, completion: @escaping (Data) -> (), failure: @escaping (NetworkErrorResponse) -> ()) {
        Alamofire.request(request.requestUrl).responseData { (response) in
            if let data = response.data, let httpResponse = response.response, httpResponse.statusCode == 200 {
                completion(data)
            } else {
                self.handleBadResponse(response: response, failure: failure)
            }
        }
    }
    
    private let imageDownloader = ImageDownloader()
    
    func makeRequest(request: AFRRequest, completion: @escaping (UIImage) -> (), failure: @escaping (NetworkErrorResponse) -> ()) {
        
        guard let url = URL(string: request.requestUrl) else {
            failure(.error(AlamofireNetworkError.incorrectRequest(request)))
            return
        }
        
        let urlRequest = URLRequest(url: url)
        imageDownloader.download(urlRequest) { (response) in
            if let data = response.value, let httpResponse = response.response, httpResponse.statusCode == 200 {
                completion(data)
            } else {
                self.handleBadResponse(response: response, failure: failure)
            }
        }
    }
    
    private func handleBadResponse<T>(response: DataResponse<T>, failure: @escaping (NetworkErrorResponse) -> ()) {
        if let httpResponse = response.response {
            failure(.badResponse(httpResponse))
        } else if let error = response.error {
            failure(.error(error))
        } else {
            failure(.error(AlamofireNetworkError.internalError))
        }
    }
}

enum AlamofireNetworkError: Error {
    case internalError
    case incorrectRequest(AFRRequest)
    
    var localizedDescription: String {
        switch self {
        case .internalError:
            return NSLocalizedString("Undefined network error", comment: "")
        case .incorrectRequest(let request):
            return NSLocalizedString("Undefined network error", comment: "") + "request : \(request.requestUrl)"
        }
        
    }
}
