//
//  NetworkProcedure.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

struct MutableQuery: Query {
    var queryItems: Array<URLQueryItem> {
        return mutableQueryItems
    }
    
    private var mutableQueryItems: Array<URLQueryItem>
    
    init(query: Query, appendedQuery: Query) {
        mutableQueryItems = query.queryItems
        mutableQueryItems.append(contentsOf: appendedQuery.queryItems)
    }
}

enum AlamofireRequest: AFRRequest {
    case searchMovie(Query)
    
    var requestUrl: String {
        switch self {
        case .searchMovie(let query):
            let config = NetworkMainConfig()
            let mainQuery = MutableQuery(query: NetworkMainQuery(params: config), appendedQuery: query)
            let procedure = ProcedureMethod.search(.movie)
            let components = RequestComponentsConstructor.request(server: config, urlMethod: procedure, query: mainQuery)
            return components.string ?? config.serverDomain
        }
    }
}

struct AlamofireImageRequest: AFRRequest {
    let config: ServerNetworkConfig
    let procedure: NetworkMethodUrl
    
    var requestUrl: String {
        let components = RequestComponentsConstructor.request(server: config, urlMethod: procedure)
        return components.string ?? config.serverDomain
    }
}
