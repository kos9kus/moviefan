//
//  ErrorDescriptor.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 23/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

protocol ErrorDescriptor {
    var title: String { get }
    var message: String { get }
    var verboseMessage: String { get }
}

extension ErrorDescriptor {
    var verboseMessage: String {
        return ""
    }
}
