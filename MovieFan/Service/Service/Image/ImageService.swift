//
//  ImageService.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 03/10/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation
import UIKit

struct ImageParam {
    let path: String
    let size: String
}

protocol ImageService {
    func image(imageParam: ImageParam, completion: @escaping (UIImage) -> (), failure: @escaping (ErrorDescriptor) -> ())
}

protocol ImagePerfomer {
    associatedtype NetworkProviderObject: NetworkProvider
    var network: Network<NetworkProviderObject> { get }
}
