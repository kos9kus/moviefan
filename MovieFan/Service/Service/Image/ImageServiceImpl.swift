//
//  ImageServiceImpl.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 03/10/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class ImageServiceImpl: ImageService, ImagePerfomer {
    
    typealias NetworkProviderObject = AlamofireNetwork

    var network: Network<NetworkProviderObject>
    
    init(networkPerfomer: Network<NetworkProviderObject>, networkConfig: ServerNetworkConfig) {
        network = networkPerfomer
        config = networkConfig
    }
    
    private let config: ServerNetworkConfig
    
    func image(imageParam: ImageParam, completion: @escaping (UIImage) -> (), failure: @escaping (ErrorDescriptor) -> ()) {
        let procedure = ProcedureMethod.image(size: imageParam.size, imagePath: imageParam.path)
        let request = AlamofireImageRequest(config: config, procedure: procedure)
        network.makeRequest(request: request, completion: completion, failure: failure)
    }
}
