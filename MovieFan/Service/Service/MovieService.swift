//
//  MoviePageModel.swift
//  MovieFan
//
//  Created by k.kusainov on 25/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol MovieService {
    func fetchMovies(pattern queryPattern: String, page pageIndex: Int, completion: @escaping (MoviePage) -> (), failure: @escaping (ErrorDescriptor) -> ())
}

protocol MoviePerfomer {
    associatedtype NetworkProviderObject: NetworkProvider
    var network: Network<NetworkProviderObject> { get }
    associatedtype ParserDecoderObject: ParserDecoder
    var parser: Parser<ParserDecoderObject> { get }
}
