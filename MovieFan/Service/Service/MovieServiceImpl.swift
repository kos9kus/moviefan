//
//  MovieServiceImpl.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 28/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class MovieServiceImpl: MovieService, MoviePerfomer {

    typealias NetworkProviderObject = AlamofireNetwork
    typealias ParserDecoderObject = JsonParser
    
    var network: Network<NetworkProviderObject>
    var parser: Parser<ParserDecoderObject>
    
    init(networkPerfomer: Network<NetworkProviderObject>, parserPerfomer: Parser<ParserDecoderObject>) {
        network = networkPerfomer
        parser = parserPerfomer
    }
    
    func fetchMovies(pattern queryPattern: String, page pageIndex: Int, completion: @escaping (MoviePage) -> (), failure: @escaping (ErrorDescriptor) -> ()) {
        let query: Query = SearchQuery(query: queryPattern, page: pageIndex)
        let request: AFRRequest = AlamofireRequest.searchMovie(query)
        self.network.makeRequest(request: request, completion: { [weak self] (data) in
            self?.parseData(data: data, completion: completion, failure: failure)
            }, failure: failure)
    }
    
    private func parseData(data: Data, completion: @escaping (MoviePage) -> (), failure: @escaping (ErrorDescriptor) -> () ) {
        self.parser.parseMoviePage(data: data, completion: completion, failure: failure)
    }
}


extension MovieServiceImpl {
    static func createMovieService() -> MovieService {
        let provider = AlamofireNetwork()
        let network = Network(networkProvider: provider)
        let jsonDecoder = JsonParser()
        let decoder = Parser(parserDecoder: jsonDecoder)
        let movieService = MovieServiceImpl(networkPerfomer: network, parserPerfomer: decoder)
        return movieService
    }
}
