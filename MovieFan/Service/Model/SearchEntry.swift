//
//  SearchEntry.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 05/10/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol SearchEntry {
    var entry: String { get }
    var dateCreated: Date { get }
    var successful: Bool { get }
}
