//
//  MoviePageImpl.swift
//  MovieFan
//
//  Created by k.kusainov on 26/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

struct MoviePageImpl: Decodable, MoviePage {
	var page: Int
	
	var totalPages: Int
	
	var totalResults: Int
	
    var results: [Movie] {
        return self.resultsDecodable
    }

    private var resultsDecodable: [MovieImpl]
	
	private enum MoviePageImplCodingKeys: String, CodingKey {
		case page = "page"
		case totalPages = "total_pages"
        case totalResults = "total_results"
        case results = "results"
	}
	
	init(from decoder: Decoder) throws {
		let allValues = try decoder.container(keyedBy: MoviePageImplCodingKeys.self)
		totalPages = try allValues.decode(Int.self, forKey: .totalPages)
		totalResults = try allValues.decode(Int.self, forKey: .totalResults)
		page = try allValues.decode(Int.self, forKey: .page)
        resultsDecodable = try allValues.decode([MovieImpl].self, forKey: .results)
	}
}

struct MovieImpl: Decodable, Movie {
    var id: Int
    
	var title: String
	
	var posterPath: String?
	
	var overview: String
	
	var releaseDate: Date?
    
    private enum MovieImplCodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case posterPath = "poster_path"
        case overview = "overview"
        case releaseDate = "release_date"
    }
    
    init(from decoder: Decoder) throws {
        let allValues = try decoder.container(keyedBy: MovieImplCodingKeys.self)
        id = try allValues.decode(Int.self, forKey: .id)
        title = try allValues.decode(String.self, forKey: .title)
        overview = try allValues.decode(String.self, forKey: .overview)
        do {
            posterPath = try allValues.decode(String.self, forKey: .posterPath)
            let releaseDateString = try allValues.decode(String.self, forKey: .releaseDate)
            releaseDate = try Date.dateFromStringFrom(format: "yyyy-mm-dd", date: releaseDateString)
        } catch {
            posterPath = nil
            releaseDate = nil
        }
        
    }
}
