//
//  MoviePage.swift
//  MovieFan
//
//  Created by k.kusainov on 26/09/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol MoviePage {
	var page: Int { get }
	var totalPages: Int { get }
	var totalResults: Int { get }
    var results: [Movie] { get }
}

protocol Movie {
    var id: Int { get }
    var title: String { get }
	var posterPath: String? { get }
	var overview: String { get }
	var releaseDate: Date? { get }
}
