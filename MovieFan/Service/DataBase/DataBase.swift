//
//  DataBase.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 05/10/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol DataBase {
    func allSearchEnries(query: String, completion: @escaping ([SearchEntry]) -> ())
    func allSearchEnries(query: String, ascending: Bool, successful: Bool, limit: Int, completion: @escaping ([SearchEntry]) -> ())
    
    func addSearchEntry(entry: SearchEntry)
    func addSearchEntry(entry: SearchEntry, failure: ((ErrorDescriptor) -> ())?)
}
