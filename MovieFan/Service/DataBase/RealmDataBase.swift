//
//  RealmDataBase.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 05/10/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation
import RealmSwift

class RealmDataBase: DataBase {
    
    enum RealmDataBaseError: ErrorDescriptor {
        case dataBaseError(Error)
        
        var title: String {
            switch self {
            case .dataBaseError(_):
                return "Data base error"
            }
        }
        
        var message: String {
            switch self {
            case .dataBaseError(let error):
                return "Error: \(error.localizedDescription)"
            }
        }
    }
    
    func allSearchEnries(query: String, completion: @escaping ([SearchEntry]) -> ()) {
        self.allSearchEnries(query: query, ascending: false, successful: true, limit: 10, completion: completion)
    }
    
    func allSearchEnries(query: String, ascending: Bool, successful: Bool, limit: Int, completion: @escaping ([SearchEntry]) -> ()) {
        DispatchQueue.global(qos: .userInitiated).async {
            let realm = try! Realm()
            var filterPredicate = "successful = \(successful)"
            if query.count > 0 {
                filterPredicate += " AND "
                filterPredicate += "entry BEGINSWITH '\(query)'"
            }
            let fetchedObjects = realm.objects(SearchEntryRealm.self).filter(filterPredicate).sorted(byKeyPath: "dateCreated", ascending: ascending).map({ (entryRealm) -> SearchEntryRealm in
                return SearchEntryRealm(value: entryRealm)
            })
            
            completion(Array(fetchedObjects.prefix(limit)))
        }
    }
    
    func addSearchEntry(entry: SearchEntry, failure: ((ErrorDescriptor) -> ())?) {
        DispatchQueue.global(qos: .userInitiated).async {
            do {
                let realm = try Realm()
                let entryRealm = SearchEntryRealm(searchEntry: entry)
                try realm.write {
                    realm.add(entryRealm, update: true)
                }
            } catch (let exp) {
                failure?(RealmDataBaseError.dataBaseError(exp))
            }
        }
    }
    
    func addSearchEntry(entry: SearchEntry) {
        self.addSearchEntry(entry: entry, failure: nil)
    }
}
