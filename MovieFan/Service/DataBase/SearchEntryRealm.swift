//
//  SearchEntryRealm.swift
//  MovieFan
//
//  Created by KONSTANTIN KUSAINOV on 05/10/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation
import RealmSwift

class SearchEntryRealm: Object, SearchEntry {
    @objc dynamic var entry: String = ""
    @objc dynamic var dateCreated: Date = Date()
    @objc dynamic var successful: Bool = false
    
    convenience init(searchEntry: SearchEntry) {
        self.init()
        entry = searchEntry.entry
        dateCreated = searchEntry.dateCreated
        successful = searchEntry.successful
    }
    
    override static func primaryKey() -> String? {
        return "entry"
    }
}
